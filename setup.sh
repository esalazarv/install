#!/usr/bin/bash
gardening=~/server
gardening_repo=https://github.com/ytake/gardening.git
clarobox_repo=https://bitbucket.org/clbox/clarobox.git
ssh_key=~/.ssh/id_rsa.pub
vagrant_yaml=~/.gardening/vagrant.yaml

directory_sync=Src
relative_sync=~/Src

hosts_file=/etc/hosts
server_ip="192.168.10.10"
project_domain=clarobox.app
project_dir=clarobox

if type "vagrant"; then
    echo "Vagrant OK";
else
    echo "Vagrant not found, you must install before configuring the development environment";
    exit;
fi

function start_server {
    cd $gardening && vagrant up && vagrant ssh
}

if [ ! -f $ssh_key ]; then
    cd ~/ && ssh-keygen -t rsa -C "string" -f $ssh_key -N ""
fi

if ! test -d $relative_sync; then
    mkdir -p $relative_sync;
    cd $relative_sync && git clone $clarobox_repo $project_dir
fi

if ! grep -q "$server_ip $project_domain" $hosts_file; then
   sudo sh -c "echo \"$server_ip $project_domain\" >> $hosts_file"
fi

if ! test -d $gardening; then
    git clone $gardening_repo $gardening && cd $gardening && bash setup.sh
    if [ -f $vagrant_yaml ]; then
        #Set ip server
        sed -i '' "s/ip: \"192.168.10.10\"/ip: \"$server_ip\"/" $vagrant_yaml

        #Change sync dir names
        sed -i '' "s/applicationPath/$directory_sync/" $vagrant_yaml

        #Setting project domain

        sed -i '' "s/- map: gardening.app/- map: $project_domain/" $vagrant_yaml
        sed -i '' "s/\/home\/vagrant\/$directory_sync\/public/\/home\/vagrant\/$directory_sync\/$project_dir\/public/" $vagrant_yaml

        #Enable mongodb
        sed -i '' "s/mongodb: false/mongodb: true/" $vagrant_yaml

        #Enable environment vars

        sed -i '' "s/# variables:/variables:/" $vagrant_yaml
        sed -i '' "s/#    - key: 'APP_ENV'/    - key: 'APP_ENV'/" $vagrant_yaml
        sed -i '' "s/#      value: 'local'/      value: 'local'/" $vagrant_yaml
        sed -i '' "s/#    - key: 'APP_DEBUG'/    - key: 'APP_DEBUG'/" $vagrant_yaml
        sed -i '' "s/#      value: 'true'/      value: 'true'/" $vagrant_yaml

        start_server;
    else
        echo "$vagrant_yaml file no exists!!";
    fi
else
    start_server;
fi
