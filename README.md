# Claro Box setup

This install and configure a vagrant box

## Requirements

* Virtual box
* Vagrant
* Git


## How to Use

First you have to clone the repository on the folder where you want it

```
$ cd ~/
$ git clone https://bitbucket.org/esalazarv/install.git install
```

### Installation

Now you have to install all the dependencies

```
$ cd ~/install
$ bash setup.sh
```

Once the installation is completed, you will automatically enter via ssh into the virtual environment and you should position yourself in the `/Src/clarobox` folder and install the composer dependencies

```
$ cd ~/Src/clarobox
$ composer install
```
